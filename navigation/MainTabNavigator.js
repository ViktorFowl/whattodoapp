import React from "react";
import { Platform } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";

import TabBarIcon from "../components/TabBarIcon";
import List from "../screens/List";
import ListDone from "../screens/ListDone";

// here we define two stack navigators for TODO and DONE lists

const ListStack = createStackNavigator({
  List: List
});

ListStack.navigationOptions = {
  tabBarLabel: "TODO",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="ios-construct" />
  )
};

const ListDoneStack = createStackNavigator({
  ListDone: ListDone
});

ListDoneStack.navigationOptions = {
  tabBarLabel: "Done",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="ios-checkmark-circle-outline" />
  )
};

// and now we take both stacks and embed them in bottom navigator
export default createBottomTabNavigator({
  ListStack,
  ListDoneStack
});

