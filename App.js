import React from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";
import { Asset, Font, Icon } from "expo";
import AppNavigator from "./navigation/AppNavigator";

export const TODO = "TODO";
export const DONE = "DONE";

// this component maintains state of the tasks

export default class App extends React.Component {
  state = {
    tasks: [
      {
        title: "what to do",
        state: TODO,
        key: String(Date.now() + Math.random())
      },
      {
        title: "add honey",
        state: TODO,
        key: String(Date.now() + Math.random())
      },
      {
        title: "eat honey",
        state: DONE,
        key: String(Date.now() + Math.random())
      }
    ]
  };

  // arrow function that marks a task with next state, currently next state is DONE
  toggleTask = (key, state) => {
    const updatedTasks = this.state.tasks.map(task => {
      if (task.key === key) {
        task.state = state;
      }
      return task;
    });

    this.setState({ tasks: updatedTasks });
  };

  // actual function for adding task
  handleTaskAddition = title => {
    const task = {
      title,
      state: TODO,
      key: String(Date.now())
    };

    this.setState({
      tasks: [...this.state.tasks, task]
    });
  };

  render() {
    return (
      <View style={styles.container}>
        {Platform.OS === "ios" && <StatusBar barStyle="default" />}
        <AppNavigator
          screenProps={{
            tasks: this.state.tasks,
            toggleTask: this.toggleTask,
            addTask: this.handleTaskAddition
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
