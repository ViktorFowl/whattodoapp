# WhatToDoApp

Hello MTAA, this is simple todo app created with expo and react-native.

### Useful links

* [nodejs] - JavaScript runtime
* [reactnative] - React Native
* [expo] - Expo

### How to run project

You need to install node.js and additionaly yarn.

```sh
$ npm install yarn
```

Clone or download this project, run `npm install` or `yarn install` in the project folder then run either `npm start` or `yarn start` and start creating your application.

```sh
$ npm start
$ yarn start
```

   [nodejs]: <https://nodejs.org/>
   [reactnative]: <https://facebook.github.io/react-native/docs/getting-started>
   [expo]: <https://expo.io>