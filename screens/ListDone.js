import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import { SwipeListView } from "react-native-swipe-list-view";
import { DONE } from "../App";

// this component displays task that are done

export default class ListDone extends Component {
  static navigationOptions = {
    title: "Done"
  };

  render() {
    // tasks are passed to this component under screenProps as can be found in App.js
    const { tasks } = this.props.screenProps;
    const done = tasks.filter(t => t.state === DONE); // we want to display only task that are done

    return (
      <View style={styles.container}>
        <SwipeListView
          useFlatList
          data={done}
          disableRightSwipe
          renderItem={(data) => (
            <View style={styles.rowFront}>
              <Text style={styles.rowText}>{data.item.title}</Text>
            </View>
          )}
          rightOpenValue={-75}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#5f27cd"
  },
  rowFront: {
    alignItems: "center",
    backgroundColor: "#ff9f43",
    justifyContent: "center",
    height: 60,
    borderBottomWidth: 1,
    borderBottomColor: "#c8d6e5"
  },
  rowText: {
    color: "#222f3e",
    fontWeight: "600",
    fontSize: 15
  }
});
