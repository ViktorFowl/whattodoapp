import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  Modal,
  TouchableOpacity,
  TextInput
} from "react-native";
import { SwipeListView } from "react-native-swipe-list-view";
import { Icon } from "expo";
import { DONE, TODO } from "../App";

// this component displays task that we want to do

export default class List extends Component {
  static navigationOptions = {
    title: "ToDo"
  };

  state = {
    addModalVisible: false
  };

  // arrow function to open modal
  toggleModal = () => {
    this.setState({ addModalVisible: !this.state.addModalVisible });
  };

  // arrow function, that handles text input from modal
  handleAddTask = () => {
    this.props.screenProps.addTask(this.state.task);
    this.setState({ task: "", addModalVisible: false });
  };

  render() {
    const { tasks, toggleTask } = this.props.screenProps;
    const done = tasks.filter(t => t.state === TODO); // we want to show only task that are TODO

    return (
      <View style={styles.container}>
        <SwipeListView
          useFlatList
          data={done}
          disableRightSwipe
          renderItem={(data) => (
            <View style={styles.rowFront}>
              <Text style={styles.rowText}>{data.item.title}</Text>
            </View>
          )}
          renderHiddenItem={(data) => (
            <TouchableOpacity
              style={styles.rowBack}
              onPress={() => toggleTask(data.item.key, DONE)}
            >
              <View style={styles.rowBack}>
                <Text>Done</Text>
              </View>
            </TouchableOpacity>
          )}
          rightOpenValue={-75}
          ListFooterComponent={
            <TouchableOpacity onPress={this.toggleModal}>
              <View style={styles.addWrapper}>
                <Icon.Ionicons
                  color={"#10ac84"}
                  name="ios-add-circle"
                  size={32}
                  style={{ marginRight: 10 }}
                />
                <Text style={styles.add}>Add to do</Text>
              </View>
            </TouchableOpacity>
          }
        />
        <Modal
          onRequestClose={this.toggleModal}
          visible={this.state.addModalVisible}
          transparent
          animationType="fade"
        >
          <View style={styles.modalHolder}>
            <View style={styles.modal}>
              <Text style={styles.modalTitle}>Add ToDo</Text>
              <TextInput
                style={styles.textInput}
                value={this.state.task}
                onChangeText={task => this.setState({ task })}
                placeholder="What should you do?"
                placeholderTextColor="#eee"
              />
              <View style={styles.buttons}>
                <TouchableOpacity
                  onPress={this.toggleModal}
                  style={styles.cancelButton}
                >
                  <Text style={styles.buttonText}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.handleAddTask}
                  style={styles.addButton}
                >
                  <Text style={styles.buttonText}>Add</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#5f27cd"
  },
  add: {
    fontSize: 18,
    fontWeight: "600",
    color: "#8395a7"
  },
  addWrapper: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30
  },
  modalHolder: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  modal: {
    backgroundColor: "#54a0ff",
    padding: 20,
    width: "80%",
    borderRadius: 5
  },
  modalTitle: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center"
  },
  textInput: {
    height: 25,
    marginTop: 20,
    borderBottomWidth: 1
  },
  buttons: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 30
  },
  cancelButton: {
    backgroundColor: "#8395a7",
    width: "45%",
    paddingVertical: 10
  },
  addButton: {
    backgroundColor: "#01a3a4",
    width: "45%",
    paddingVertical: 10
  },
  buttonText: {
    textAlign: "center",
    color: "#c8d6e5"
  },
  rowFront: {
    alignItems: "center",
    backgroundColor: "#ff9f43",
    justifyContent: "center",
    height: 60,
    borderBottomWidth: 1,
    borderBottomColor: "#feca57"
  },
  rowText: {
    color: "#222f3e",
    fontWeight: "600",
    fontSize: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#c8d6e5"
  },
  rowBack: {
    alignItems: "center",
    backgroundColor: "#feca57",
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingRight: 10
  }
});

